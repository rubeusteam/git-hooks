#!/bin/bash

project=$1 # [automacao, app-retencao-totvs, ps-gerenciado]
environment=$2 # [test, development]

version=0.2-Beta

case $project in
    app-retencao-totvs)
        cd ..
    ;;
    ps-gerenciado)
        cd ..
    ;;
esac

if [ -e .git/hooks/pre-commit ];
then
    PRE_COMMIT_EXISTS=1
else
    PRE_COMMIT_EXISTS=0
fi

#remove old utils and tools
mkdir -p .git/hooks/utils
mkdir -p .git/hooks/tools
rm -rf .git/hooks/utils/
rm -rf .git/hooks/tools/

#create folders for utils and tools again
mkdir -p .git/hooks/utils 
mkdir -p .git/hooks/tools 

#copying new utils and tools for git-hooks
case $project in
    app-retencao-totvs)
        cp ./back/vendor/rubeus/git-hooks/src/utils/* .git/hooks/utils/
        cp ./back/vendor/rubeus/git-hooks/src/tools/* .git/hooks/tools/
        sed "s/__PATH__/.\/back\//" -i .git/hooks/tools/phpa
        sed "s/__PATH__/.\/back\//" -i .git/hooks/tools/phpcs
    ;;
    ps-gerenciado)
        cp ./back/vendor/rubeus/git-hooks/src/utils/* .git/hooks/utils/
        cp ./back/vendor/rubeus/git-hooks/src/tools/* .git/hooks/tools/
        sed "s/__PATH__/.\/back\//" -i .git/hooks/tools/phpa
        sed "s/__PATH__/.\/back\//" -i .git/hooks/tools/phpcs
    ;;
    *)
        cp ./vendor/rubeus/git-hooks/src/utils/* .git/hooks/utils/
        cp ./vendor/rubeus/git-hooks/src/tools/* .git/hooks/tools/
        sed "s/__PATH__/.\//" -i .git/hooks/tools/phpa
        sed "s/__PATH__/.\//" -i .git/hooks/tools/phpcs
        sed "s/__PATH__/.\//" -i .git/hooks/tools/insights
    ;;
esac

case $project in
    automacao)
        echo "importing for automacao"
        if [ "$environment" = "development" ]; then
            cp ./vendor/rubeus/git-hooks/src/projects/automacao/develop/pre-commit .git/hooks/pre-commit  
        else 
            cp ./vendor/rubeus/git-hooks/src/projects/automacao/test/pre-commit .git/hooks/pre-commit  
        fi
    ;;
    app-retencao-totvs)
        echo "importing for app retenção totvs"
        if [ "$environment" = "development" ]; then
            cp ./back/vendor/rubeus/git-hooks/src/projects/app-retencao-totvs/develop/pre-commit .git/hooks/pre-commit  
        else 
            cp ./back/vendor/rubeus/git-hooks/src/projects/app-retencao-totvs/test/pre-commit .git/hooks/pre-commit  
        fi
    ;;
    ps-gerenciado)
        echo "importing for app processo seletivo integrado gerenciavel"
        if [ "$environment" = "development" ]; then
            cp ./back/vendor/rubeus/git-hooks/src/projects/ps-gerenciado/develop/pre-commit .git/hooks/pre-commit  
        else 
            cp ./back/vendor/rubeus/git-hooks/src/projects/ps-gerenciado/test/pre-commit .git/hooks/pre-commit  
        fi
    ;;
    *)
        echo "importing default"
        cp ./vendor/rubeus/git-hooks/src/pre-commit .git/hooks/pre-commit
    ;;
esac

chmod +x .git/hooks/pre-commit

if [ "$PRE_COMMIT_EXISTS" = 0 ];
then
    echo -e "\n\t Pre-commit git hook is installed! "
else
    echo -e "\n\t Pre-commit git hook is updated! "
fi

echo -e "\n\t >>> Version: $version <<< \n\n"