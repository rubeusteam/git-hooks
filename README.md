# README #

Antes de começar, leia atentamente este guia.

### Para que este repositório serve? ###

* Breve Introdução
* Versão
* Dependências
* Como Usar
* Contribuição


### Breve Introdução ###

Git hooks são scripts que o Git executa quando uma determinada ação ocorre no repositório. Eles permitem que você automatize tarefas personalizáveis de acordo com o seu workflow de desenvolvimento.
    
Esse é um recurso nativo do Git, ou seja, não é necessário realizar nenhum download para utilizá-lo.

Alguns exemplos interessantes de uso são:
    - Deploy da aplicação após um git push no repositório;
    - Checagem de sintaxe ou padrões de código antes de um git commit;
    - gerar documentação antes do envio das modificações;

Para ativar um Hook, é necessário apenas criar um arquivo e salvá-lo na pasta .git/hooks/. Quando você executar  um determinado comando no repositório ( commit, push e etc.. ), o Git verificará dentro do diretório hooks se há um script associado para ser executado. O nome do arquivo é pré-definido pelo Git.

A linguagem padrão é  Shell Script. Mas, você pode escrever seus hooks em qualquer linguagem de script, desde que o interpretador da linguagem esteja disponível nas variáveis de ambiente do sistema.


### Versão ###

#### dev-main ####

Este é um projeto ainda no seu início de desenvolvimento, não há uma versão estável nomeada, para todos os efeitos, utilizaremos a nomenclatura **dev-main** para se referir a versão atual em desenvolvimento.


### Dependências ###

    Sistema Operacional Unix
    php: =>5.4
    rskuipers/php-assumptions: ^0.8.0
    squizlabs/php_codesniffer: 3.*


### Como Usar ###

#### Configuração ####

Adicione o seguinte conjunto de scripts ao **composer.json** para a instalação padrão.

    "scripts": {
        "install-hooks": ["sh ./vendor/rubeus/git-hooks/src/setup.sh"],
        "post-install-cmd": ["@install-hooks"],
        "post-update-cmd": ["@install-hooks"]
    }

Você pode personalizar as ferramentas que serão utilizadas de acordo com o seu projeto, as opções são:

Projeto | Ambiente | Uso 
:--------| :------- |:----
automacao | development | `"sh ./vendor/rubeus/git-hooks/src/setup.sh automacao development"`
automacao | test | `"sh ./vendor/rubeus/git-hooks/src/setup.sh automacao test"`
app-retencao-totvs | development | `"sh ./vendor/rubeus/git-hooks/src/setup.sh app-retencao-totvs development"`
app-retencao-totvs | test | `"sh ./vendor/rubeus/git-hooks/src/setup.sh app-retencao-totvs test"`
ps-gerenciado | development | `"sh ./vendor/rubeus/git-hooks/src/setup.sh ps-gerenciado development"`
ps-gerenciado | test | `"sh ./vendor/rubeus/git-hooks/src/setup.sh ps-gerenciado test"`

#### Instalação ####

Execute a instalação dos scripts git-hooks deste projeto com o seguinte comando:

    composer require --dev "rubeus/git-hooks:dev-master"

**Nota**: Não se esqueça do parâmetro --dev ao executar essa instalação em seus projetos, os scripts deste projeto não devem ser utilizados em servidores de produção.

#### Atualização ####

##### Passo 1 #####

    Confira se há novas configurações disponíveis e em caso positivo visite seu arquivo **composer.json** para alterá-las.

##### Passo 2 #####

    Execute o `composer update` normalmente para atualização dos scripts git-hooks, quando disponíveis.


### Contribuição ###

No momento você pode contribuir com esse projeto da seguinte forma:
    
* Sugestões
* Escrita de Testes
* Code Review
* Outros guidelines